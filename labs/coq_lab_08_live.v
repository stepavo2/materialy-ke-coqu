Require Import Arith ZArith.

(** Znaceni pro funkcni typy [A -> B]
je specialni pripad obecne kvantifikace.
Funkci [f : A -> B] muzeme s otypovanim
zapsat i takto:
[f : forall (a : A), B]. *)

Definition naslednik: forall (n : nat), nat :=
  (fun (n : nat) => S n).

(** Coq automaticky zapise typ funkce [naslednik]
v prijemnejsim tvaru: *)
Check naslednik.

Print bool.

Inductive pracovni_den : Set :=
  | pondeli
  | utery
  | streda
  | ctvrtek
  | patek.

Print pracovni_den.

(** Podivejte se na indukcni a rekursivni principy
vygenerovane Coqem. *)
Check pracovni_den_ind.
Check pracovni_den_rec.
Check pracovni_den_rect.

(** Pomoci [pracovni_den_rec] definujte funkci
[vikend_za_n_dni : pracovni_den -> nat].
*)
Definition vikend_za_n_dni :
  forall (d : pracovni_den), nat :=
  pracovni_den_rec (fun (d : pracovni_den) => nat)
  5 4 3 2 1.

Compute vikend_za_n_dni ctvrtek.

(** Jaky je typ funkci [bool_ind] a [bool_rec]
vygenerovanych Coqem pro typ [bool]? *)
Print bool.
Check bool_ind.
Check bool_rec.

(** note to self: do the stupid proof first*)
Lemma vsechny_pracovni_dny :
  forall (d : pracovni_den),
    d = pondeli \/ d = utery \/ d = streda
    \/ d = ctvrtek \/ d = patek.
Proof.
  intro d.
  destruct d;
  auto 5.
Qed.

(* do the more automated proof now*)
Lemma vsechny_pracovni_dny' :
  forall (d : pracovni_den),
    d = pondeli \/ d = utery \/ d = streda
    \/ d = ctvrtek \/ d = patek.
Proof.
Admitted.

(** show the pracovni_den_ind proof now*)
Lemma vsechny_pracovni_dny'' :
  forall (d : pracovni_den),
    d = pondeli \/ d = utery \/ d = streda
    \/ d = ctvrtek \/ d = patek.
Proof.
Abort.

(** Slozitejsi induktivni definice
se zajimavejsimi konstruktory *)
Inductive vozidlo : Set :=
  (* argument: pocet sedadel *)
  | bicykl : nat -> vozidlo
  (* argumenty: pocet sedadel a kol*)
  | motorove : nat -> nat -> vozidlo.

(** Funkce vracejici pocet kol a sedadel *)
Definition pocet_kol (v : vozidlo) :=
  match v with
  | bicykl _ => 2
  | motorove _ k => k
  end.

Definition pocet_sedadel (v : vozidlo) :=
  match v with
  | bicykl s => s
  | motorove s k => s
  end.

(** Coq vygeneruje induktivni a rekursivni
princip pro [vozidlo]: *)
Check vozidlo_ind.
Check vozidlo_rec.

(** Pattern matching muze byt i slozitejsi:
uvnitr dukazu pouzivame zavisly (dependent)
pattern matching. *)
Lemma alespon_1_den_do_vikendu :
  forall (d : pracovni_den), 1 <= vikend_za_n_dni d.
Proof.
  intro d.
  destruct d; simpl.
  all: auto 1 with arith.
Qed.

(** Jak vypada dukazovy term? *)
Print alespon_1_den_do_vikendu.

(** Mame funkci vracejici dalsi pracovni den: *)
Definition dalsi_pracovni_den
  (d : pracovni_den) : pracovni_den :=
  match d with
  | pondeli => utery
  | utery => streda
  | streda => ctvrtek
  | ctvrtek => patek
  | patek => pondeli
  end.

(** Budeme potrebovat resit situace, kdy jedna z nasich
hypotez je tvaru [c_1 = c_2], kde
[c_1] a [c_2] jsou dva ruzne konstruktory
nejakeho induktivniho typu.
(Tj. budeme potrebovat *rozlisit* [c_1] a [c_2].)

Priklad: *)
Lemma streda_je_po_utery_test :
  forall (d : pracovni_den),
    dalsi_pracovni_den d = streda -> d = utery.
Proof.
  intros d H.
  destruct d; try reflexivity.
Abort.

(** Resenim je taktika [discriminate]: *)
Lemma test_discriminate :
  pondeli = patek -> utery = ctvrtek.
Proof.
  intro H.
  discriminate H.
Qed.

(** Nyni dokazte pozadovane lemma: *)
Lemma streda_je_po_utery :
  forall (d : pracovni_den),
    dalsi_pracovni_den d = streda -> d = utery.
Proof.
  intros d H.
  destruct d; simpl in H;
  discriminate H || reflexivity.
Qed.

(** Jak taktika [discriminate] funguje uvnitr? *)
(** [change] je taktika, ktera umi soucasny cil
zmenit na jiny, ale ekvivalentni cil. *)
(** note to self: ukazat!*)
Lemma pondeli_neni_utery :
  ~ (pondeli = utery).
Proof.
  unfold not.
  intro H.
  change ( (fun d => match d with
                     | pondeli => False
                     | _ => True
                     end) pondeli).
  rewrite H.
  apply I.
Qed.


(** Ulohy: *)
(** Dokazte, ze [true <> false] v typu [bool]. *)
(** Pouzijte podobny postup jako vyse,
ne taktiku [discriminate].*)
Lemma true_neni_false : ~ (true = false).
Proof.
  intro H.
  change ( (fun (b : bool) =>
            match b with
            | true => True
            | false => False
            end) false).
  rewrite <- H.
  apply I.
Qed.

(** Dokazte, ze v typu [vozidlo] se
zadny bicykl nerovna zadnemu motorovemu vozidlu.*)
Lemma odliseni_bicyklu_a_motorovych:
  forall (m n p : nat), ~ (bicykl m = motorove n p).
Proof.
  intros m n p H.
  change ( (fun v =>
          match v with
          | bicykl _ => False
          | motorove _ _ => True
          end) (bicykl m)).
  rewrite H.
  apply I.
Qed.

Lemma odliseni_bicyklu_a_motorovych':
  forall (m n p : nat), ~ (bicykl m = motorove n p).
Proof.
  intros m n p H.
  discriminate H.
Qed.

(** Injektivita konstruktoru *)
(** Budeme potrebovat resit situace, kdy jedna z nasich
hypotez je tvaru [c x y z = c u v w], kde
[c] je konstruktor nejakeho induktivniho typu.
Meli bychom byt schopni odvodit rovnosti
[x = u], [y = v], [z = w].
(Tj. budeme potrebovat ukazat,
ze konstruktor [c] je *injektivni*.) *)

Lemma stejna_kola_stejne_sedadel:
  forall m n, bicykl m = bicykl n -> m = n.
Proof.
  intros m n H.
  injection H as K.
  assumption.
Qed.

(** Na jakem principu funguje [injection]? *)
(** Vyuzijte funkci [pocet_sedadel]
a taktiku [change], abyste lemma dokazali
bez pouziti [injection]. *)
Lemma stejna_kola_stejne_sedadel':
  forall m n, bicykl m = bicykl n -> m = n.
Proof.
  intros m n H.
  change ((pocet_sedadel (bicykl m)) = n).
  rewrite H.
  simpl.
  reflexivity.
Qed.

(** Pripomenme si typ prirozenych cisel. *)
Print nat.

(** Podivejme se na induktivni princip,
ktery Coq pro prirozena cisla vygeneroval. *)
Check nat_ind.

Check nat_ind :
(*Pro vsechny vlastnosti P prirozenych cisel *)
forall P : nat -> Prop,
(* Pokud ma 0 vlastnost P, *)
       P 0 ->
(* Pokud pro vsechna prirozena cisla n plati:
pokud n ma vlastnost P, pak (1 + n) ma vlastnost P, *)
       (forall n : nat, P n -> P (S n)) ->
(* pak pro vsechna prirozena cisla n plati:
n ma vlastnost P. *)
       forall n : nat, P n.

(** Uzitim [nat_ind] dokazte rovnost
[n + 0 = n]. *)
Lemma n_plus_0_n : forall n, n + 0 = n.
Proof.
  intro n.
  refine (nat_ind (fun (m : nat) => m + 0 = m) (eq_refl 0) _ n).
  intros m H.
  simpl.
  rewrite H.
  reflexivity.
Qed.

(** Definujte rekursivne funkci
[sum_f : nat -> (nat -> Z) -> Z],
ktera pro hodnotu [n]
a funkci [f : nat -> Z]
secte vsechny hodnoty [(f m)]
pro [m < n].*)
Fixpoint sum_f (n : nat) (f : nat -> Z) : Z :=
  match n with
  | 0 => 0
  | S n' => f n' + sum_f n' f
  end.

Compute sum_f 5 Z.of_nat.

(** Definujme induktivni datovy typ binarnich stromu
s celymi cisly v nelistovych vrcholech: *)
Inductive Z_btree : Set :=
  | Z_leaf : Z_btree
  |

(** Jaky je typ induktivniho principu pro binarni stromy? *)
Check Z_btree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu. *)

Fixpoint sum_all_values (t : Z_btree) : Z.
Admitted.

Fixpoint zero_present (t : Z_btree) : Z.
Admitted.

(** Definujte induktivni typ representujici jazyk
vyrokove logiky bez promennych, jen s konstantami
pravda a nepravda, a s logickymi spojkami negace,
konjunkce, disjunkce a implikace. *)

(** ----------------------------- *)

(** Alternativni zpusob definice binarnich stromu: *)
Inductive Z_fbtree : Set.

(** Definujte funkci vracejici levou (ci pravou) vetev
binarniho stromu [t: Z_fbtree].
(U listu vratte list.) *)

(** Prostudujte induktivni princip pro [Z_fbree].
Popiste vlastnimi slovy, co rika. *)
Check Z_fbtree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu
typu [Z_fbtree]. *)

(*--------------------*)

(** Stromy s nekonecnym vetvenim *)

Inductive Z_inf_branch_tree : Set.

(** Definujte funkci scitajici vsechny hodnoty
ve stromu typu [Z_inf_branch_tree], ktere jsou
ulozeny v indexech mensich nez [n]. *)
(** Vyuzijte vami naprogramovanou funkci [sum_f]. *)

(** Definujte funkci rozhodujici, zda strom typu
[Z_inf_branch_tree] obsahuje hodnotu [0] v nekterem
z vrcholu s indexem mensim nez [n].*)

(* ------------------------------- *)

(** Uz jsme videli polymorfni typy,
induktivni definice mohou ale byt
parametrizovany jinymi nez typovymi hodnotami. *)
(** Definice stromu, ktery v sobe pripousti
hodnoty ohranicene cislem [n]: *)
Inductive ltree (n : nat) : Set :=
  | lleaf : ltree n
  | lnode : forall (p : nat),
            p <= n ->
            ltree n -> ltree n -> ltree n.

(** Jelikoz je [n] v predchozi definici
parametrem, musi byt [n] pouzito u vsech
vyskytu [ltree] v definici. *)

(** Coq umoznuje i volnejsi definice:
binarni strom s hodnotami ve vsech vrcholech,
+ *podminka*: obe vetve stromu musi mit stejnou vysku. *)
Inductive htree (A : Set) : nat -> Set :=
  | hleaf : A -> htree A 0
  | hnode : forall (n : nat),
            A ->
            htree A n ->
            htree A n ->
            htree A (S n).

(** Zkuste pochopit, co rika indukcni princip
pro typ [htree]: *)
Check htree_ind.

(** Definujte funkci [htree_to_btree]. *)
(** Definujte funkci [invert], ktera
invertuje strom [t: htree A n].
Coq za vas dokonce zvladne doplnit
dependent pattern matching konstrukt. *)
Fixpoint invert (A : Set) (n : nat) (t : htree A n) : htree A n.
Admitted.

Print invert.

(** Induktivne definovat lze nejen
"mnoziny", ale i predikaty: *)

(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(** Induktivni definice relace "mensi nebo rovno": *)
Inductive le (n : nat) : nat -> Prop :=
  | le_n : le n n
  | le_S : forall (m : nat), le n m -> le n (S m).

(** Induktivni definice usporadaneho listu: *)
Inductive sorted (A : Set) (R : A -> A -> Prop) :
  list A -> Prop :=
  | sorted0 : sorted A R nil
  | sorted1 : forall (a : A), sorted A R (cons a nil)
  | sorted2 : forall (a b : A) (l : list A),
              R a b ->
              sorted A R (cons b l) ->
              sorted A R (cons a (cons b l)).

Arguments sorted {A}.

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
Admitted.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
Admitted.

(** Dokazte "inverzni" lemma: *)
Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
Admitted.

(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
Admitted.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 1.
Proof.
Admitted.

(** Obcas je treba pouzit indukci na predikat: *)
Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
  intros n m H.
  induction H.
  - trivial.
  - intro K. simpl. apply ev_SS. apply IHev, K.
Qed.

(** Zde je alternativni definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  split.
Admitted.