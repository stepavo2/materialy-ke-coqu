(**
Tento materiál je z ~90 % převzat z Logical Foundations;
místy zkrácen, místy doplněn.

Autoři

Benjamin C. Pierce
Arthur Azevedo de Amorim
Chris Casinghino
Marco Gaboardi
Michael Greenberg
Cătălin Hriţcu
Vilhelm Sjöberg
Brent Yorgey
with
Loris D'Antoni, Andrew W. Appel, Arthur Charguéraud, Michael Clarkson, Anthony Cowley, Jeffrey Foster, Dmitri Garbuzov, Olek Gierczak, Michael Hicks, Ranjit Jhala, Ori Lahav, Greg Morrisett, Jennifer Paykin, Mukund Raghothaman, Chung-chieh Shan, Leonid Spesivtsev, Andrew Tolmach, Philip Wadler, Stephanie Weirich, Li-Yao Xia, and Steve Zdancewic

https://softwarefoundations.cis.upenn.edu/lf-current/index.html
*)

(** Zkoušíme velmi jednoduché programování
ve funkcionálním programovacím jazyku Gallina,
seznamujeme se se základními příkazy Coqu. *)

(** Modulem vymezujeme obor platnosti nasich definic;
mimo modul [OurBool] se funkce [function] z tohoto modulu
vola jako [OurBool.function].*)
Module OurBool.

(** Definice nového datového typu [bool] ("boolean").
Je to "výčtový" typ, speciální případ induktivního datového typu. *)
Inductive bool : Set :=
  | true
  | false.

(** [Check] kontroluje typ výrazu. *)
Check true.
Check bool.

(** Jednoduchá definice nové funkce pracující s [bool]: *)
Definition negb (b : bool) : bool :=
  match b with
  | true => false
  | false => true
  end.

Check negb.

(** Definice bez využití parametru [b : bool]. *)
Definition negb_no_par : bool -> bool :=
  fun (b : bool) =>
  match b with
  | true => false
  | false => true
  end.

Check negb_no_par.

(** [Print] tiskne definici termu. *)
Print negb_no_par.
Print negb.

(** [Compute] provádí evaluaci termu. *)
Compute (negb true).
Compute (negb (negb true)).

(** [Example] je téměř synonymem příkazů [Lemma] a [Theorem]. *)
Example test_negb: (negb false) = true.
Proof.
  simpl.
  reflexivity.
Qed.

(** Definujte booleovské funkce [andb] a [orb]. *)
Definition andb (b1 : bool) (b2 : bool) : bool :=
  match b1 with
  | true => b2
  | false => false
  end.

Definition orb (b1 : bool) (b2 : bool) : bool :=
  match b1 with
  | true => true
  | false => b2
  end.

(** Odkomentujte následující testy a prověřte své definice. *)
Example test_orb1 : (orb true false) = true.
Proof.
  simpl. reflexivity.
Qed.

Example test_orb2 : (orb false false) = false.
Proof.
  simpl. reflexivity.
Qed.

Example test_orb3 : (orb false true) = true.
Proof.
  simpl. reflexivity.
Qed.

Example test_orb4 : (orb true true) = true.
Proof.
  simpl. reflexivity.
Qed.

(** Coq umožňuje specifikovat vlastní syntax pro definované funkce. *)
Notation "x && y" := (andb x y).
Notation "x || y" := (orb x y).


Example test_orb5 : false || false || true = true.
Proof. simpl. reflexivity. Qed.


(** Konstrukt [if-then-else]: *)
Definition negb' (b:bool) : bool :=
  if b then false
  else true.

Compute (negb (negb false)).

(** Definujte [andb'] a [orb'] pomocí [if-then-else]. *)
Definition andb' (b1:bool) (b2:bool) : bool :=
  if b1 then b2
  else false.

Definition orb' (b1:bool) (b2:bool) : bool :=
  if b1 then true
  else b2.

End OurBool.


Check OurBool.negb.

(** [negb] (a [bool]) je ve standardní knihovně: *)
Check bool.
Print bool.
Check negb.
Print negb.


(** Krátký výlet do definice přirozených čísel: *)
Module OurNat.

Inductive nat : Set :=
  | O : nat
  | S (n : nat) : nat.

Check nat.

Compute (S (S (S O))).

(** Naprogramujte funkci predchudce prirozeneho cisla.
Predchudcem nuly pro nas bude nula. *)
Definition pred (n : nat) : nat :=
  match n with
  | O => O
  | S n => n
  end.

Compute (pred (S (S (S O)))).

End OurNat.

(** Přirozená čísla jsou součástí standardní knihovny: *)
Print nat.

(** Coq unární notaci pro přirozená čísla převádí do dekadické: *)
Check (S (S (S O))).

(** Pro rekursivní definice používáme [Fixpoint]: *)
Fixpoint even (n : nat) : bool :=
  match n with
  | O => true
  | S 0 => false
  | S (S n) => even n
  end.

(** Definujte funkci [odd] ("je liché"). *)
Definition odd (n : nat) := negb (even n).

(** Znovu zavádíme modul, aby se naše definice nekřížily
s definicemi ze standardní knihovny. *)

Module NatFunctions.

  Fixpoint plus (n : nat) (m : nat) : nat :=
    match n with
    | O => m
    | S n' => S (plus n' m)
    end.

Compute (plus 3 2).

Fixpoint mult (n m: nat) : nat :=
  match n with
  | O => O
  | S n' => plus (mult n' m) m
  end.

Compute (mult 3 4).

(* Dvojitý match oddělujeme čárkou: *)
Fixpoint minus (n m:nat) : nat :=
  match n, m with
  | O   , _    => O
  | S _ , O    => n
  | S n', S m' => minus n' m'
  end.

Compute (minus 10 6).

End NatFunctions.

(** Naprogramujte faktoriál: *)
Fixpoint factorial (n : nat) : nat :=
  match n with
  | O => 1
  | S n' => mult n (factorial n')
  end.

Example test_factorial1: (factorial 3) = 6.
Proof. reflexivity. Qed.
Example test_factorial2: (factorial 5) = (mult 10 12).
Proof. reflexivity. Qed.

(** [bool]-rovnost čísel je uživatelsky definovaná: naprogramujte ji. *)
Fixpoint eqb (n m : nat) : bool :=
  match n, m with
  | O, O => true
  | S n', S m' => eqb n' m'
  | _, _ => false
  end.

(** Naprogramujte booleovskou funkci "je menší nebo rovno". *) 
Fixpoint leb (n m : nat) : bool :=
  match n, m with
  | O, _ => true
  | S n', S m' => leb n' m'
  | _, _ => false
  end.

Example test_leb1: leb 2 2 = true.
Proof. simpl. reflexivity. Qed.
Example test_leb2: leb 2 4 = true.
Proof. simpl. reflexivity. Qed.
Example test_leb3: leb 4 2 = false.
Proof. simpl. reflexivity. Qed.

(** Specifikujeme vlastní syntax pro
booleovskou rovnost a nerovnost přirozených čísel. *)
Notation "x =? y" := (eqb x y) (at level 70) : nat_scope.
Notation "x <=? y" := (leb x y) (at level 70) : nat_scope.
Example test_leb3': (4 <=? 2) = false.
Proof. simpl. reflexivity. Qed.

(** Chytře definujte funkci "je menší než". *)
Definition ltb (n m : nat) : bool := (n <=? m) && negb (n =? m).

Notation "x <? y" := (ltb x y) (at level 70) : nat_scope.
Example test_ltb1: (ltb 2 2) = false.
Proof. reflexivity. Qed.
Example test_ltb2: (ltb 2 4) = true.
Proof. reflexivity. Qed.
Example test_ltb3: (ltb 4 2) = false.
Proof. reflexivity. Qed.

(** Pro rychlejší: binární přirozená čísla. *)
Inductive bin : Type :=
  | Z
  | B0 (n : bin)
  | B1 (n : bin).
  
(** Definujte funkci "přičtení jedničky"
a "převod do unární soustavy". *)
Fixpoint incr (m:bin) : bin :=
  match m with
  | Z => B1 Z
  | B0 n => B1 n
  | B1 n => B0 (incr n)
  end.

Fixpoint bin_to_nat (m:bin) : nat :=
  match m with
  | Z => O
  | B0 n => mult 2 (bin_to_nat n)
  | B1 n => plus 1 (mult 2 (bin_to_nat n))
  end.
  
Example test_bin_incr1 : (incr (B1 Z)) = B0 (B1 Z).
Proof. reflexivity. Qed.
Example test_bin_incr2 : (incr (B0 (B1 Z))) = B1 (B1 Z).
Proof. reflexivity. Qed.
Example test_bin_incr3 : (incr (B1 (B1 Z))) = B0 (B0 (B1 Z)).
Proof. reflexivity. Qed.
Example test_bin_incr4 : bin_to_nat (B0 (B1 Z)) = 2.
Proof. reflexivity. Qed.
Example test_bin_incr5 :
        bin_to_nat (incr (B1 Z)) = 1 + bin_to_nat (B1 Z).
Proof. reflexivity. Qed.
Example test_bin_incr6 :
        bin_to_nat (incr (incr (B1 Z))) = 2 + bin_to_nat (B1 Z).
Proof. reflexivity. Qed.
