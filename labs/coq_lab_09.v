Require Import Arith ZArith Bool.

(** [sum_f : nat -> (nat -> Z) -> Z]
pro hodnotu [n] a funkci [f : nat -> Z]
secte vsechny hodnoty [(f m)]
pro [m < n].*)
Fixpoint sum_f (n : nat) (f : nat -> Z) : Z :=
  match n with
  | 0 => 0
  | S n' => f n' + sum_f n' f
  end.

(** Definujme induktivni datovy typ binarnich stromu
s celymi cisly v nelistovych vrcholech: *)
Inductive Z_btree : Set :=
| Z_leaf
| Z_bnode : Z -> Z_btree -> Z_btree -> Z_btree.

(** Jaky je typ induktivniho principu pro binarni stromy? *)
Check Z_btree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu. *)

Fixpoint sum_all_values (t : Z_btree) : Z :=
  match t with
  | Z_leaf => 0
  | Z_bnode x x0 x1 => x + sum_all_values x0 + sum_all_values x1
  end.

Fixpoint zero_present (t : Z_btree) : bool :=
  match t with
  | Z_leaf => false
  | Z_bnode x x0 x1 => (orb (Z.eqb x 0) (orb (zero_present x0) (zero_present x1)))
  end.

(** Definujte induktivni typ representujici jazyk
vyrokove logiky bez promennych, jen s konstantami
pravda a nepravda, a s logickymi spojkami negace,
konjunkce, disjunkce a implikace. *)

Inductive PL_Formula : Prop :=
| TRUE : PL_Formula
| FALSE : PL_Formula
| NOT : PL_Formula -> PL_Formula
| AND : PL_Formula -> PL_Formula -> PL_Formula
| OR : PL_Formula -> PL_Formula -> PL_Formula
| IMPL : PL_Formula -> PL_Formula -> PL_Formula.

(** ----------------------------- *)

(** Alternativni zpusob definice binarnich stromu: *)
Inductive Z_fbtree : Set :=
| Z_fleaf : Z_fbtree
| Z_fnode : Z -> (bool -> Z_fbtree) -> Z_fbtree.

(** Definujte funkci [fleft_son] ([fright_son])
vracejici levou (ci pravou) vetev
binarniho stromu [t: Z_fbtree].
(U listu vratte list.) *)
Definition fleft_child (t: Z_fbtree) : Z_fbtree :=
  match t with
  | Z_fleaf => Z_fleaf
  | Z_fnode _ children => children true
  end.

(** Prostudujte induktivni princip pro [Z_fbree].
Popiste vlastnimi slovy, co rika. *)
Check Z_fbtree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu
typu [Z_fbtree].
([fsum_all_values] a [fzero_present])*)

Fixpoint fsum_all_values (t: Z_fbtree) : Z :=
  match t with
  | Z_fleaf => 0
  | Z_fnode x f => x + fsum_all_values (f true) + fsum_all_values (f false)
  end.

Fixpoint fzero_present (t: Z_fbtree) : bool :=
  match t with
  | Z_fleaf => false
  | Z_fnode x f => (x =? 0)%Z || fzero_present (f true) || fzero_present (f false)
  end.

(*--------------------*)

(** Stromy s nekonecnym vetvenim *)
Inductive Z_inf_branch_tree : Set :=
| Z_inf_leaf : Z_inf_branch_tree
| Z_inf_node : Z -> (nat -> Z_inf_branch_tree) -> Z_inf_branch_tree.

(** Definujte funkci scitajici vsechny hodnoty
ve stromu typu [Z_inf_branch_tree], ktere jsou
ulozeny v indexech mensich nez [n]. *)

Fixpoint infsum (n: nat) (t: Z_inf_branch_tree) : Z :=
  match t with
  | Z_inf_leaf => 0
  | Z_inf_node x f => x + sum_f n (fun i => infsum n (f i))
  end.

(** Vyuzijte vami naprogramovanou funkci [sum_f]. *)

(** Definujte funkci rozhodujici, zda strom typu
[Z_inf_branch_tree] obsahuje hodnotu [0] v nekterem
z vrcholu s indexem mensim nez [n].*)


(* ------------------------------- *)

(** Uz jsme videli polymorfni typy,
induktivni definice mohou ale byt
parametrizovany jinymi nez typovymi hodnotami. *)
(** Definice stromu, ktery v sobe pripousti
hodnoty ohranicene cislem [n]: *)
Inductive ltree (n : nat) : Set :=
  | lleaf : ltree n
  | lnode : forall (p : nat),
            p <= n ->
            ltree n -> ltree n -> ltree n.

(** Jelikoz je [n] v predchozi definici
parametrem, musi byt [n] pouzito u vsech
vyskytu [ltree] v definici. *)

(** Coq umoznuje i volnejsi definice:
binarni strom s hodnotami ve vsech vrcholech,
+ *podminka*: obe vetve stromu musi mit stejnou vysku. *)
Inductive htree (A : Set) : nat -> Set :=
  | hleaf : A -> htree A 0
  | hnode : forall (n : nat),
            A ->
            htree A n ->
            htree A n ->
            htree A (S n).
Arguments hleaf {A}.
Arguments hnode {A}.

(** Zkuste pochopit, co rika indukcni princip
pro typ [htree]: *)
Check htree_ind.

(** Definujte funkci [htree_to_btree]. *)
Fixpoint htree_to_btree {n: nat} (h: htree Z n) : Z_btree :=
  match h with
  | hleaf x => Z_leaf
  | hnode _ x l r => Z_bnode x (htree_to_btree l) (htree_to_btree r)
  end.

(** Definujte funkci [invert], ktera
invertuje strom [t: htree A n].
Coq za vas dokonce zvladne doplnit
dependent pattern matching konstrukt. *)
Fixpoint invert (A : Set) (n : nat) (t : htree A n) : htree A n :=
  match t with
  | hleaf x => hleaf x
  | hnode n x l r => hnode n x r l
  end.

Print invert.

Example htree1 : htree nat 1 := hnode _ 3 (hleaf 2) (hleaf 4).

(** Induktivne definovat lze nejen
"mnoziny", ale i predikaty: *)

(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(** Induktivni definice relace "mensi nebo rovno": *)
Inductive le (n : nat) : nat -> Prop :=
  | le_n : le n n
  | le_S : forall (m : nat), le n m -> le n (S m).

(** Induktivni definice usporadaneho listu: *)
Inductive sorted (A : Set) (R : A -> A -> Prop) :
  list A -> Prop :=
  | sorted0 : sorted A R nil
  | sorted1 : forall (a : A), sorted A R (cons a nil)
  | sorted2 : forall (a b : A) (l : list A),
              R a b ->
              sorted A R (cons b l) ->
              sorted A R (cons a (cons b l)).

Arguments sorted {A}.

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
  apply ev_SS. apply ev_SS. apply ev_0.
Qed.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
  intros n.
  induction n.
  - simpl. constructor.
  - simpl. constructor. assumption.
Qed.

(** Dokazte "inverzni" lemma: *)
Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
  intros n H.
  induction H.
  - left. reflexivity.
  - right. exists n. split. reflexivity. assumption.
Qed.

(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
Admitted.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
Admitted.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 1.
Proof.
Admitted.


(** Dokazte s vyuzitim [inversion] :*)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
Admitted.

(** Dokazte s vyuzitim [inversion] :*)
Theorem ev5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
Admitted.

(** Alternativni, "neinduktivni" definice sudosti: *)
Definition Even x := exists n : nat, x = double n.

(** Obcas je treba pouzit indukci podle predikatu: *)
(** (nezapomenme na [unfold]!) *)
Lemma ev_Even : forall n,
  ev n -> Even n.
Proof.
  intros n H.
  induction H.
  - unfold Even.
    exists 0.
    reflexivity.
  - unfold Even in *.
    destruct IHev.
    rewrite H0.
    exists (S x).
    reflexivity.
Qed.

(** Dokazte indukci podle predikatu: *)
Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
Admitted.

(** Dokazte obraceny smer ekvivalence nasich dvou
definici sudosti. *)
Lemma Even_ev : forall n, Even n -> ev n.
Proof.
Admitted.

(** Zde je dalsi mozna definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou
induktivnich definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  split.
Admitted.

(** Dokazte indukci (ale podle ceho?): *)
Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
Admitted.
