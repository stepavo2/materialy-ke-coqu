Section negace_kontradikce.

  Hypotheses (P Q : Prop).

  (** Zde nutno znát pravidla pro negaci a False. *)

  (** Jak se chová negace?
  [~ P] je zkratka za [P -> False].
  Můžeme tedy používat taktiku [intro].*)

  (** Z [False] lze odvodit cokoli (to je pravidlo sporu "ex falso quodlibet").
  Můžeme využít taktiku [exfalso].*)
  Lemma not_contrad :  ~(P /\ ~P).
  Proof.
    unfold not.
    intros [HP HNP].
    apply HNP, HP.
  Qed.

  Lemma or_and_not : (P \/ Q) /\ ~P -> Q.
  Proof.
    unfold not.
    intros [[HP | HQ] HNP].
    - exfalso. apply HNP, HP.
    - assumption.
  Qed.

  Lemma de_morgan_1 : ~(P \/ Q) -> ~P /\ ~Q.
  Proof.
    unfold not.
    intros H.
    split; intros h.
    - apply H. left; assumption.
    - apply H. right; assumption.
  Qed.

  Lemma de_morgan_2 : ~P /\ ~Q -> ~(P \/ Q).
  Proof.
    unfold not.
    intros [HNP HNQ] [H | H].
    all: [> apply HNP | apply HNQ]. all: assumption.
  Qed.

  Lemma de_morgan_3 : ~P \/ ~Q -> ~(P /\ Q).
  Proof.
    unfold not.
    intros [HNP | HNQ].
    - intros [HP _]. apply HNP, HP.
    - intros [_ HQ]. apply HNQ, HQ.
  Qed.

  Lemma not_not_exm : ~ ~ (P \/ ~ P).
  Proof.
    unfold not. intros H.
    cut (P -> False).
    - intros NP.
      apply H. right. assumption.
    - intros HP.
      apply H. left. assumption.
  Qed.

End negace_kontradikce.

Section min_logika_programy.

  (** Nyní budeme důkazy programovat! *)
  (** Je nutno předvést syntax pro aplikaci funkcí a pro abstrakci (anonymní funkce). *)
  Hypotheses P Q R S : Prop.

  Lemma test (p : P) (f : P -> Q) : Q.
  Proof. exact (f p). Qed.

  Lemma test' (p : P) : (P -> Q) -> Q.
  Proof.
    exact (fun f => f p).
  Qed.

  Lemma test'' : P -> (P -> Q) -> Q.
  Proof.
    exact (fun p f => f p).
  Qed.

  Print test''.

  Lemma test_tactics : P -> (P -> Q) -> Q.
  Proof.
    intros p f.
    apply f.
    apply p.
  Qed.

  Print test_tactics.

  Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)).
  Proof.
    exact (fun f q p => q (f p)).
  Qed.

  Lemma id_P : P -> P.
  Proof.
    exact (fun p => p).
  Qed.

  Lemma id_PP : (P -> P) -> P -> P.
  Proof.
    exact (fun f => f).
  Qed.

  Lemma ignore_Q : (P -> R) -> P -> Q -> R.
  Proof.
    exact (fun f p _ => f p).
  Qed.

  Lemma delta_impR : (P -> Q) -> P -> P -> Q.
  Proof.
    exact (fun f p _ => f p).
  Qed.

  (* Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)). *)
  (* Proof *)
  (* Admitted. *)

  Lemma delta_imp : (P -> P -> Q) -> P -> Q.
  Proof.
    exact (fun f p => f p p).
  Qed.

  Lemma imp_dist : (P -> Q -> R) -> (P -> Q) -> P -> R.
  Proof.
    exact (fun f g p => f p (g p)).
  Qed.

  Lemma imp_perm : (P -> Q -> R) -> Q -> P -> R.
  Proof.
    exact (fun f q p => f p q).
  Qed.

  Lemma diamond : (P -> Q) -> (P -> R) -> (Q -> R -> S) -> P -> S.
  Proof.
    exact (fun f g h p => h (f p) (g p)).
  Qed.    

  Lemma weak_peirce : ((((P -> Q) -> P) -> P) -> Q) -> Q.
  Proof.
    intros H.
    apply H.
    intros H0.
    apply H0.
    intros H1.
    apply H.
    exact (fun _ => H1).
  Qed.

End min_logika_programy.
