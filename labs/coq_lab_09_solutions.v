Require Import Arith ZArith.

(** [sum_f : nat -> (nat -> Z) -> Z]
pro hodnotu [n] a funkci [f : nat -> Z]
secte vsechny hodnoty [(f m)]
pro [m < n].*)
Fixpoint sum_f (n : nat) (f : nat -> Z) : Z :=
  match n with
  | 0 => 0
  | S n' => f n' + sum_f n' f
  end.

(** Definujme induktivni datovy typ binarnich stromu
s celymi cisly v nelistovych vrcholech: *)
Inductive Z_btree : Set :=
  | Z_leaf : Z_btree
  | Z_bnode : Z -> Z_btree -> Z_btree -> Z_btree.

(** Jaky je typ induktivniho principu pro binarni stromy? *)
Check Z_btree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu. *)

Fixpoint sum_all_values (t : Z_btree) : Z :=
  match t with
  | Z_leaf => 0
  | Z_bnode z t1 t2 => z + (sum_all_values t1) + (sum_all_values t2)
  end.


Fixpoint zero_present (t : Z_btree) : bool :=
  match t with
  | Z_leaf => false
  | Z_bnode 0 _ _ => true
  | Z_bnode _ t1 t2 => if zero_present t1 then true else zero_present t2
  end.

(** Definujte induktivni typ representujici jazyk
vyrokove logiky bez promennych, jen s konstantami
pravda a nepravda, a s logickymi spojkami negace,
konjunkce, disjunkce a implikace. *)
Inductive PL_Formula :=
  | TRUE
  | FALSE
  | NOT (f : PL_Formula)
  | AND (f1 : PL_Formula) (f2 : PL_Formula)
  | OR (f1 : PL_Formula) (f2 : PL_Formula)
  | IMPL (f1 : PL_Formula) (f2 : PL_Formula).
(** ----------------------------- *)

(** Alternativni zpusob definice binarnich stromu: *)
Inductive Z_fbtree : Set :=
  | Z_fleaf : Z_fbtree
  | Z_fnode : Z -> (bool -> Z_fbtree) -> Z_fbtree.

(** Definujte funkci [fleft_son] ([fright_son])
vracejici levou (ci pravou) vetev
binarniho stromu [t: Z_fbtree].
(U listu vratte list.) *)
Definition fleft_son (t : Z_fbtree) :=
  match t with
  | Z_fleaf => Z_fleaf
  | Z_fnode _ f => f true
  end.

(** Prostudujte induktivni princip pro [Z_fbtree].
Popiste vlastnimi slovy, co rika. *)
Check Z_fbtree_ind.

(** Definujte funkce scitajici hodnoty ve stromu
a zjistujici pritomnost nuly mezi hodnotami ve stromu
typu [Z_fbtree].
([fsum_all_values] a [fzero_present])*)
Fixpoint fsum_all_values (t : Z_fbtree) : Z :=
  match t with
  | Z_fleaf => 0
  | Z_fnode z f => z + fsum_all_values (f true)
                     + fsum_all_values (f false)
  end.

Fixpoint fzero_present (t : Z_fbtree) : bool :=
  match t with
  | Z_fleaf => false
  | Z_fnode 0 _ => true
  | Z_fnode _ f => if fzero_present (f true) then true
                   else fzero_present (f false)
  end.
(*--------------------*)

(** Stromy s nekonecnym vetvenim *)

Inductive Z_inf_branch_tree : Set :=
  | Z_inf_leaf : Z_inf_branch_tree
  | Z_inf_node : Z ->
                 (nat -> Z_inf_branch_tree) ->
                 Z_inf_branch_tree.

(** Definujte funkci [n_sum_all_values]
scitajici vsechny hodnoty ve stromu typu
[Z_inf_branch_tree], ktere jsou ulozeny
v indexech mensich nez [n]. *)
(** Vyuzijte vami naprogramovanou funkci [sum_f]. *)
Fixpoint n_sum_all_values
  (n : nat) (t : Z_inf_branch_tree) : Z :=
  match t with
  | Z_inf_leaf => 0
  | Z_inf_node z f => z +
                      sum_f n (fun x =>
                      (n_sum_all_values n (f x)))
  end.

(** Definujte funkci rozhodujici, zda strom typu
[Z_inf_branch_tree] obsahuje hodnotu [0] v nekterem
z vrcholu s indexem mensim nez [n].*)
Fixpoint num_zero_present (n : nat) (t : Z_inf_branch_tree) : Z:=
  match t with
  | Z_inf_leaf => 0
  | Z_inf_node 0 _ => 1
  | Z_inf_node _ f => sum_f n (fun x =>
                      (num_zero_present n (f x)))
  end.

Definition nzero_present (n : nat) (t : Z_inf_branch_tree) :=
  match (num_zero_present n t) with
  | 0%Z => false
  | _ => true
  end.
(* ------------------------------- *)

(** Uz jsme videli polymorfni typy,
induktivni definice mohou ale byt
parametrizovany jinymi nez typovymi hodnotami. *)
(** Definice stromu, ktery v sobe pripousti
hodnoty ohranicene cislem [n]: *)
Inductive ltree (n : nat) : Set :=
  | lleaf : ltree n
  | lnode : forall (p : nat),
            p <= n ->
            ltree n -> ltree n -> ltree n.

(** Jelikoz je [n] v predchozi definici
parametrem, musi byt [n] pouzito u vsech
vyskytu [ltree] v definici. *)

(** Coq umoznuje i volnejsi definice:
binarni strom s hodnotami ve vsech vrcholech,
+ *podminka*: obe vetve stromu musi mit stejnou vysku. *)
Inductive htree (A : Set) : nat -> Set :=
  | hleaf : A -> htree A 0
  | hnode : forall (n : nat),
            A ->
            htree A n ->
            htree A n ->
            htree A (S n).

(** Zkuste pochopit, co rika indukcni princip
pro typ [htree]: *)
Check htree_ind.

(** Definujte funkci [htree_to_btree]. *)
Fixpoint htree_to_btree (n : nat) (t : htree Z n) : Z_btree :=
  match t with
  | hleaf _ z => Z_bnode z Z_leaf Z_leaf
  | hnode _ n' z t1 t2 => Z_bnode z
                        (htree_to_btree n' t1)
                        (htree_to_btree n' t2)
  end.
(** Definujte funkci [invert], ktera
invertuje strom [t: htree A n].
Coq za vas dokonce zvladne doplnit
dependent pattern matching konstrukt. *)
Fixpoint invert (A : Set) (n : nat) (t : htree A n) : htree A n :=
  match t with
  (* match t in (htree _ x) return (htree _ x) with *)
  | hleaf _ a => hleaf A a
  | hnode _ y a t1 t2 => hnode A y a (invert A y t2) (invert A y t1)
  end.

Print invert.

(** Induktivne definovat lze nejen
"mnoziny", ale i predikaty: *)

(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(** Induktivni definice relace "mensi nebo rovno": *)
Inductive le (n : nat) : nat -> Prop :=
  | le_n : le n n
  | le_S : forall (m : nat), le n m -> le n (S m).

(** Induktivni definice usporadaneho listu: *)
Inductive sorted (A : Set) (R : A -> A -> Prop) :
  list A -> Prop :=
  | sorted0 : sorted A R nil
  | sorted1 : forall (a : A), sorted A R (cons a nil)
  | sorted2 : forall (a b : A) (l : list A),
              R a b ->
              sorted A R (cons b l) ->
              sorted A R (cons a (cons b l)).

Arguments sorted {A}.

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
  apply ev_SS.
  apply ev_SS.
  apply ev_0.
Qed.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
  intros n.
  induction n.
  - simpl. apply ev_0.
  - simpl. apply ev_SS, IHn.
Qed.

Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
  intros n H.
  destruct H as [ | m K].
  - left. reflexivity.
  - right. exists m. split.
    + reflexivity.
    + assumption.
Qed.

(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
  intros n H.
  destruct H as [ | n' H'].
  - simpl. apply ev_0.
  - simpl. apply H'.
Qed.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
  intros n H.
  apply ev_inversion in H.
  destruct H as [H_0 | H_1].
  - discriminate.
  - destruct H_1 as [w K].
    destruct K as [K_0 K_1].
    injection K_0 as E.
    rewrite E.
    apply K_1.
Qed.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
  intros n H.
  inversion H.
  - assumption.
Qed.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 1.
Proof.
  intros H.
  inversion H.
Qed.


(** Dokazte s vyuzitim [inversion] :*)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
  intros n H.
  inversion H as [| n' H' Heq].
  inversion H' as [| n'' H'' H'eq]. apply H''.
Qed.

(** Dokazte s vyuzitim [inversion] :*)
Theorem ev5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
  intro H.
  inversion H. inversion H1. inversion H3.
Qed.

(** Alternativni, "neinduktivni" definice sudosti: *)
Definition Even x := exists n : nat, x = double n.

(** Obcas je treba pouzit indukci podle predikatu: *)
(** (nezapomenme na [unfold]!) *)
Lemma ev_Even : forall n,
  ev n -> Even n.
Proof.
  intros n H.
  induction H.
  - unfold Even.
    exists 0.
    reflexivity.
  - unfold Even in *.
    destruct IHev.
    rewrite H0.
    exists (S x).
    reflexivity.
Qed.

Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
  intros n m H.
  induction H.
  - trivial.
  - intro K. simpl. apply ev_SS. apply IHev, K.
Qed.

(** Dokazte obraceny smer ekvivalence nasich dvou
definici sudosti. *)
Lemma Even_ev : forall n, Even n -> ev n.
Proof.
  intros n H.
  destruct H.
  rewrite H.
  apply ev_double.
Qed.

(** Zde je dalsi mozna definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou
induktivnich definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  intro n.
  split.
  - intro H.
    induction H as [| | n m En Em IH].
    + apply ev_0.
    + apply ev_SS. apply ev_0.
    + apply ev_sum.
      * assumption.
      * assumption.
  - intro H.
    induction H.
    + apply ev'_0.
    + apply (ev'_sum 2 n).
      * apply ev'_2.
      * apply IHev.
Qed.


(** Dokazte: pokud je soucet a jeden scitanec sudy,
je i druhy scitanec sudy.
(Podle ktereho tvrzeni vest indukci?)*)
Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
  intros n m Hsum Hn.
  induction Hn.
  - apply Hsum.
  - simpl in Hsum.
    apply IHHn.
    inversion Hsum.
    assumption.
Qed.