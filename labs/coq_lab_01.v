(**
Disclaimer: výběr úloh jsem do značné míry přejal z knihy Coq'Art od Yvese Bertota a Pierra Castérana.
*)



(** * Ukázkový příklad

Vyzkoušíme si odvození jednoduchého důsledku z výrokové logiky.

Založením nové sekce [ukazkovy_priklad] dáváme najevo, že námi zavedené hypotézy mají pouze lokální platnost v dané sekci.*)

Section ukazkovy_priklad.

(** Nejprve deklarujeme výroky, které budeme dále používat. *)
  Hypotheses (uraz_hlavy ztratim_praci rozvedu_se budu_alkoholik : Prop).

(** Deklarací výše jsme netvrdili, že dané výroky platí. Pokud např. chceme tvrdit, že jsem utrpěl úraz hlavy ([uraz_hlavy]), musíme platnost tohoto výroku dosvědčit (zde spíše předpokládat) zavedením [U : uraz_hlavy]. *)

  Hypotheses (U_Z : uraz_hlavy -> ztratim_praci) (ZR_A : ztratim_praci /\ rozvedu_se -> budu_alkoholik) (U : uraz_hlavy).

(** Nyní tedy předpokládáme tři tvrzení:
- Pokud se mi stal úraz hlavy, rozvedu se.
- Pokud ztratím práci a rozvedu se, budu alkoholik.
- Stal se mi úraz hlavy.

Plyne z těchto třech tvrzení následující tvrzení?
- Pokud se rozvedu, budu alkoholik.

Ukážeme, že ano.
*)
  Theorem R_A : rozvedu_se -> budu_alkoholik.
  Proof using U U_Z ZR_A.
    intro R.
    apply ZR_A.
    split.
    - apply U_Z.
      apply U.
    - apply R.
  Qed.

End ukazkovy_priklad.

(**
Důkaz tvrzení z předpokladů:
- Máme hypotézy a chceme odvodit závěr.
- Našim hypotézám říkáme dohromady kontext.
- Zapisujeme do tzv. *sekventu*: Gamma |- phi
- Důkaz v Coq budujeme interaktivně tak, že se snažíme splnit *cíl* (goal) (sekvent, který chceme dokázat), a cíl měníme taktikami.
- Používáme *zaváděcí* (introduction) taktiky podle toho, jaká je hlavní *logická spojka* v závěru, nebo *eliminační* (elimination) taktiky na naše hypotézy.

*)

(** Minimální výroková logika

Používáme pouze atomické výroky a implikace.
Používáme pouze tři odvozovací pravidla (uvést).
- Pravidlo předpokladu
- Eliminace implikace (modus ponens)
- Zavedení implikace

Nakreslit na tabuli.
*)

Section minimalni_logika.

  Hypotheses P Q R S : Prop.

  Lemma imp_trans : (P -> Q) -> ((Q -> R) -> (P -> R)).
  Proof.
    intros H H0 H1.
    apply H0. apply H. apply H1.
  Qed.

  Lemma id_P : P -> P.
  Proof.
    intros H. apply H.
  Qed.

  Lemma id_PP : (P -> P) -> P -> P.
  Proof.
    intros H. apply H.
  Qed.

  Lemma ignore_Q : (P -> R) -> P -> Q -> R.
  Proof.
    intros H H0 H1.
    apply H. apply H0.
  Qed.


  Lemma delta_impR : (P -> Q) -> P -> P -> Q.
  Proof.
    intros H H0 H1.
    apply H. apply H0.
  Qed.


  (** Zde (a dále) bude třeba použít [cut] (případně pokročilé vlastnosti [apply]).*)
  Lemma delta_imp : (P -> P -> Q) -> P -> Q.
  Proof.
    intros H H0.
    apply H.
    - apply H0.
    - apply H0.
  Qed.

  Lemma imp_dist : (P -> Q -> R) -> (P -> Q) -> P -> R.
  Proof.
    intros H H0 H1.
    cut Q.
    - apply H.
      assumption.
    - apply H0.
      assumption.
  Qed.
  (*   apply H. *)
  (*   - assumption. *)
  (*   - apply H0. assumption. *)
  (* Qed. *)

  (* Zde používám [apply] nestandardně (viz referenční manuál). *)
  Lemma imp_perm : (P -> Q -> R) -> Q -> P -> R.
  Proof.
    intros H H0 H1.
    apply (H H1 H0).
  Qed.


  (** Pro zájemce, klidně můžeme přeskočit: *)
  Lemma diamond : (P -> Q) -> (P -> R) -> (Q -> R -> S) -> P -> S.
  Proof.
    intros H H0 H1 H2.
    apply (H1 (H H2) (H0 H2)).
  Qed.

  Lemma weak_peirce : ((((P -> Q) -> P) -> P) -> Q) -> Q.
  Proof.
    intros H.
    apply H.
    intros H0.
    apply H0.
    intros H1.
    apply H.
    intros H2.
    assumption.
  Qed.

End minimalni_logika.

Section intuicionisticka_vyrokova_logika.

  (** Ukázat pravidla pro ostatní spojky. *)

  Hypotheses A B C P Q R S T : Prop.

  (** Zde už nutno znát pravidla pro konjunkci. *)
  Lemma and_assoc : P /\ (Q /\ R) -> (P /\ Q) /\ R.
  Proof.
    intros H.
    destruct H.
    destruct H0.
    split.
    - split.
      + apply H.
      + apply H0.
    - apply H1.
  Qed.

  Lemma and_assoc' : P /\ (Q /\ R) -> (P /\ Q) /\ R.
  Proof.
    intros [PQ RS] [HP HR].
    split.
    - apply PQ, HP.
    - apply RS, HR.
  Qed.


  Lemma decurrying : (A -> (B -> C)) -> (A /\ B -> C).
  Proof.
    intros H [HA HB].
    apply H; assumption.
  Qed.


  Lemma currying : (A /\ B -> C) -> (A -> (B -> C)).
  Proof.
    intros H HA HB.
    apply H. split; assumption.
  Qed.

  (** Zde už nutno znát pravidla pro disjunkci. *)
  Lemma or_comm : A \/ B -> B \/ A.
  Proof.
    intros [HA | HB]; [ right | left ]; assumption.
  Qed.

  Lemma either_implies : ((A \/ B) -> C) -> (A -> C) /\ (B -> C).
  Proof.
    intros H.
    split; intros H0; apply H; [left | right]; assumption.
  Qed.


  Lemma either_implies' : (A -> C) /\ (B -> C) -> ((A \/ B) -> C).
  Proof.
    intros [HAC HBC] [HA | HB].
    - apply HAC, HA.
    - apply HBC, HB.
  Qed.

  (** Zde nutno znát pravidla pro negaci a False. *)
  Lemma not_contrad :  ~(P /\ ~P).
  Proof.
    intros [HP HNP].
    apply HNP, HP.
  Qed.

  Lemma or_and_not : (P \/ Q) /\ ~P -> Q.
  Proof.
    intros [[HP | HQ] HNP].
    - exfalso. apply not_contrad, (conj HP HNP).
    - apply HQ.
  Qed.

  Lemma not_not_exm : ~ ~ (P \/ ~ P).
  Proof.
    unfold not.
    intros H.
    cut (P -> False).
    - intros NP. apply H. right. assumption.
    - intros HP. apply H. left. assumption.
  Qed.

  Lemma de_morgan_1 : ~(P \/ Q) -> ~P /\ ~Q.
  Proof.
    unfold not.
    intros H. split. all: intros p. all: apply H. all: [> left | right]. all: apply p.
  Qed.

  Lemma de_morgan_2 : ~P /\ ~Q -> ~(P \/ Q).
  Proof.
    intros [NP NQ] H.
    destruct H.
    - apply NP, H.
    - apply NQ, H.
  Qed.

  Lemma de_morgan_3 : ~P \/ ~Q -> ~(P /\ Q).
  Proof.
    intros [NP | NQ]. all: intros [HP HQ]. all: [> apply NP, HP | apply NQ, HQ].
  Qed.

End intuicionisticka_vyrokova_logika.



(* Section logicke_taktiky.

Hypotheses (A B C: Prop).

  Section predpoklad.

  Hypothesis (a : A).

  Lemma A_holds : A.
  Proof.
    assumption.
  Qed.

  End predpoklad.

  Section modus_ponens.

  Hypotheses (a_b : A -> B) (a : A).

  Lemma B_plati: B.
  Proof.
    cut A.
    - assumption.
    - assumption.
  Qed.

  End modus_ponens.

  Section zavedeni_implikace.

  Lemma A_implikuje_A : A -> A.
  Proof.
    intro a.
    assumption.
  Qed.

  End zavedeni_implikace.

  Section zavedeni_konjunkce.

  Lemma A_a_B_plati : A -> B -> A /\ B.
  Proof.
    intros a b. (* intro a; intro b. *)
    split.
    - assumption.
    - assumption.
  Qed.

  Hypotheses (a_b : A -> B) (a_c : A -> C).

  Lemma z_A_plyne_B_a_C : A -> B /\ C.
  Proof.
    intro a.
    split.
    - cut A.
      + assumption.
      + assumption.
      (** Použijeme [assumption] na všechny podcíle vzniklé z [cut A]. *)
    - cut A; assumption.
  Qed.

  Lemma z_A_plyne_B_a_C' : A -> B /\ C.
  Proof.
    intro a.
    split.
    - apply a_b.
      assumption.
    - apply a_c.
      assumption.
  Qed.

  End zavedeni_konjunkce.

End logicke_taktiky.
*)
