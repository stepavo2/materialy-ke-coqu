(** Vyrazne vybrano z kapitol [Lists] a [Poly]
knihy Logical Foundations! (todo: add link) *)

Set Warnings "-notation-overridden".

Module playground.

Inductive natprod : Type :=
  | pair (n1 n2 : nat).

Check pair 5 4.

Definition fst (p : natprod) :=
  match p with
  | pair n1 n2 => n1
  end.

Check fst.

Compute fst (pair 2 4).

Definition snd (p : natprod) :=
  match p with
  | pair n1 n2 => n2
  end.

Notation "( x , y )" := (pair x y).

Check (2, 3).

Compute snd (2, 3).

Definition swap (p : natprod) : natprod.
Admitted.

Lemma pairing_lemma : forall (n m : nat),
  (n, m) = (fst (n, m), snd (n, m)).
Admitted.

Lemma pair_constructed_from_destructed: forall (p: natprod),
  p = (fst p, snd p).
Admitted.

Lemma snd_fst_swap : forall (p : natprod),
  (snd p, fst p) = (swap p).
Admitted.

(** List prirozenych cisel *)

Inductive natlist :=
  | nil
  | cons (n : nat) (l : natlist).

Definition mujlist := (cons 3 (cons 2 nil)).

Notation "x :: l" := (cons x l)
                     (at level 60, right associativity).

Print mujlist.

Fixpoint repeat (n count : nat) : natlist.
Admitted.

Fixpoint length (l : natlist) : nat.
Admitted.

Fixpoint append (l1 l2 : natlist) : natlist.
Admitted.

Notation "x ++ y" := (append x y)
                     (right associativity, at level 60).

(**
--------------------------------------------------
Cviceni pro narocnejsi: implementujte nasledujici.

(* Vybere z listu nenulove prvky. *)
Fixpoint nonzeros (l:natlist) : natlist.

(* Alternuje prvky prvniho a druheho listu. *)
Fixpoint alternate (l1 l2 : natlist) : natlist.
--------------------------------------------------
*)


(** Spojovani listu je asociativni: *)
Theorem app_assoc : forall l1 l2 l3 : natlist,
  (l1 ++ l2) ++ l3 = l1 ++ (l2 ++ l3).
Admitted.

(** Implementujte otoceni listu.
(Nemusite resit casovou efektivitu sve implementace.) *)
Fixpoint reverse (l : natlist) : natlist.
Admitted.

(** Delka spojeni dvou seznamu je souctem delek spojovanych seznamu. *)
Theorem append_length : forall l1 l2 : natlist,
  length (l1 ++ l2) = (length l1) + (length l2).
Admitted.

(** Delka otoceneho seznamu se rovna delce puvodniho seznamu. *)
Theorem reverse_length : forall l : natlist,
  length (reverse l) = length l.
Admitted.


(** Vybirani [n]-teho prvku v listu muze skoncit katastrofou,
pokud je [n] vetsi nez pocet prvku v listu. *)

Inductive natoption : Type :=
  | Some (n : nat)
  | None.

(** Funkce vybere [n]-ty prvek jako [Some n], jinak vrati [None]. *)
Fixpoint nth (l:natlist) (n:nat) : natoption :=
  match l, n with
  | nil, _ => None
  | h :: _, O => Some h
  | _ :: t, S n' => nth t n'
  end.

Notation "[ x ; .. ; y ]" := (cons x .. (cons y nil) ..).

Example test_nth1 : nth [4;5;6;7] 0 = Some 4.
Proof. reflexivity. Qed.
Example test_nth2 : nth [4;5;6;7] 3 = Some 7.
Proof. reflexivity. Qed.
Example test_nth3 : nth [4;5;6;7] 9 = None.
Proof. reflexivity. Qed.

End playground.

(** Coq umoznuje definovat *polymorfni* datove typy.
Nemusime pro kazdy datovy typ psat novou definici
listu pro dany typ. *)

Inductive list (X : Type) : Type :=
  | nil
  | cons (x : X) (l : list X).

Check (nil nat) : list nat.
Check (cons nat 3 (nil nat)) : list nat.

(** Nechutne cviceni: *)

Module MumbleGrumble.

Inductive mumble : Type :=
  | a
  | b (x : mumble) (y : nat)
  | c.

Inductive grumble (X:Type) : Type :=
  | d (m : mumble)
  | e (x : X).

(** Ktere z nasledujicich termu jsou elementy [grumble X]
pro nejaky typ X?
      - [d (b a 5)]
      - [d mumble (b a 5)]
      - [d bool (b a 5)]
      - [e bool true]
      - [e mumble (b c 0)]
      - [e bool (b c 0)]
      - [c] *)
      
End MumbleGrumble.

(** Polymorfni implementace funkce [repeat]
s typovymi anotacemi: *)
Fixpoint repeat (X : Type) (x : X) (count : nat) : list X :=
  match count with
  | 0 => nil X
  | S count' => cons X x (repeat X x count')
  end.

(** Co se stane, kdyz nebudeme typy anotovat? *)
Fixpoint repeat' X x count : list X :=
  match count with
  | 0        => nil X
  | S count' => cons X x (repeat' X x count')
  end.

Check repeat.
Check repeat'.
(** Coq se snazi typy sam odvozovat (provadi *typovou inferenci* ). *)

(** Pouzitim zoliku [_] muzeme dale Coq nutit odvozovat typy: *)
Fixpoint repeat'' X x count : list X :=
  match count with
  | 0        => nil _
  | S count' => cons _ x (repeat'' _ x count')
  end.

(** Tim si muzeme usnadnit praci: *)
Definition list123 :=
  cons nat 1 (cons nat 2 (cons nat 3 (nil nat))).

Definition list123' :=
  cons _ 1 (cons _ 2 (cons _ 3 (nil _))).

(** To je stale otravne. At Coq implicitne typy vyvozuje. *)
Arguments nil {X}.
Arguments cons {X}.
Arguments repeat {X}.

Definition list123'' := cons 1 (cons 2 (cons 3 nil)).

(** Notace pro listy, tentokrat polymorfni. *)
Notation "x :: y" := (cons x y)
                     (at level 60, right associativity).
Notation "[ ]" := nil.
Notation "[ x ; .. ; y ]" := (cons x .. (cons y []) ..).
Notation "x ++ y" := (app x y)
                     (at level 60, right associativity).

(** Polymorfni pary *)

Inductive prod (X Y : Type) : Type :=
| pair (x : X) (y : Y).

(** Typove argumenty nastavime jako implicitni. *)
Arguments pair {X} {Y}.

(** Zavedeme notaci pro pary. *)
Notation "( x , y )" := (pair x y).

Notation "X * Y" := (prod X Y) : type_scope.

(** Znovu implementujte zakladni funkce pro praci s pary. *)
Definition fst {X Y : Type} (p : X * Y) : X.
Admitted.

Definition snd {X Y : Type} (p : X * Y) : Y.
Admitted.

Fixpoint combine {X Y : Type} (lx : list X) (ly : list Y)
           : list (X*Y).
Admitted.

(** Implementujte funkci rozdelujici list paru na par listu. *)
Fixpoint split {X Y : Type} (l : list (X*Y)) : (list X) * (list Y).
Admitted.

Example test_split:
  split [(1,false);(2,false)] = ([1;2],[false;false]).
Proof.
  reflexivity.
Qed.