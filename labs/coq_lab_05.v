Section unarni_predikaty.

  Context (U : Set) (P Q R : U -> Prop).
  
  (** Priklady s obecnymi kvantifikatory. *)

  (** K praci s obecnym kvantifikatorem
pouzivame taktiky
[intro] (popripade [intros]) a [apply]. *)

  Lemma antecedent_out (a : U) :
    (forall x, P a -> Q x) -> (P a -> forall z, Q z).
  Proof.
    intros H.
    intros Pa.
    intros z0.
    apply (H z0).
    apply Pa.
  Qed.
  
  Lemma antecedent_out' (a : U) :
    (forall x, P a -> Q x) -> (P a -> forall z, Q z).
  Proof.
    intros H pa z.
    pose proof (H z) as K.
    pose proof (K pa) as L.
    assumption.
  Qed.

  Lemma factor_forall_out :
    (forall x, P x) /\ (forall y, Q y) ->
    (forall z, P z /\ Q z).
  Proof.
    intros H z.
    destruct H as [HP HQ].
    split.
    - apply (HP z).
    - pose proof (HQ z) as K.
      assumption.
  Qed.
   
  Lemma allPQ_allP_allQ : 
    (forall x : U, P x -> Q x) ->
    (forall y, P y) ->
    forall z, Q z.
  Proof.
    intros H H0 z.
    apply H. apply H0.
  Qed.
   
  Lemma P_or_Q_but_not_P
   (H : forall x, P x \/ Q x)
   (K : forall y, ~ P y) :
   forall x, Q x.
  Proof.
    intros x.
    destruct (H x) as [HP | HQ].
    - exfalso. apply (K _ HP).
    - assumption.
  Qed.
  
  Lemma PQ_QR_PR
    (H : forall x, P x -> Q x)
    (K : forall x, Q x -> R x) :
    forall x, P x -> R x.
  Proof.
    intros x H0.
    apply (K _ (H _ H0)).
  Qed.
  
  (** Priklady s existencnimi kvantifikatory. *)
  
  (** K praci s existencnim kvantifikatorem pouzivame taktiky
[exists] a [destruct]. *)

  (** Priklady s existencnimi kvantifikatory. *)

  (** K praci s existencnim kvantifikatorem
pouzivame taktiky [exists] a [destruct]. *)

  Lemma exists_consequent
    (a : U) (H : exists x, P a -> Q x) :
    P a -> exists y, Q y.
  Proof.
    intros H0.
    destruct H as [x H1].
    exists x. apply (H1 H0).
  Qed.
  
  Lemma factor_exists_in_conj :
    (exists x, P x /\ Q x) ->
    (exists y, P y) /\ (exists z, Q z).
  Proof.
    intros [x [Px Qx]].
    split; exists x; assumption.
  Qed.
  
  Lemma factor_exists_in_disj :
    (exists x, P x \/ Q x) ->
    (exists y, P y) \/ (exists z, Q z).
  Proof.
    intros [x [Px | Qx]].
    - left. exists x. apply Px.
    - right. exists x. apply Qx.
  Qed.
  
End unarni_predikaty.

Section binarni_predikaty.

  Context (U : Set) (R : U -> U -> Prop).
  
  Lemma all_diagonal :
    (forall x, forall y, R x y) -> forall z, R z z.
  Proof.
    intros H z.
    apply H.
  Qed.
  
  Lemma technical (H : forall x y, R x y) :
    forall z, (R z z /\ forall t, R t z).
  Proof.
    intros z. split.
    - apply all_diagonal, H.
    - intros t. apply H.
  Qed.
  
  Lemma ex_x_y_Rxy_Ryx (u : U) :
    exists x y, R x y -> R y x.
  Proof.
    exists u, u. intros a. assumption.
  Qed.
  
End binarni_predikaty.


Section smisene_ulohy.

  Context (U : Set) (P Q S : U -> Prop).

  Lemma mix1
    (H : forall x, P x -> Q x)
    (K : exists y, P y) :
    exists z, Q z.
  Proof.
    destruct K as [y Py].
    exists y. apply (H _ Py).
  Qed.
  
  Lemma mix2 (H : forall x, Q x -> S x) (K : exists y, P y /\ Q y) :
    exists z, P z /\ S z.
  Proof.
    destruct K as [y [Py Qy]].
    exists y. split; [assumption | apply H, Qy].
  Qed.
  
  Lemma mix3 (H : exists x, P x) (K : forall y z, P y -> Q z) :
    forall t, Q t.
  Proof.
    intros t.
    destruct H as [x H].
    apply K with (y:=x), H.
  Qed.
  
End smisene_ulohy.


Section slovni_uloha.

  (*
Every young and healthy person likes baseball.

Every active person is healthy.

Someone is young and active.

Therefore, someone likes baseball.
*)
  Context (U : Set) (A B Y H : U -> Prop).

  Lemma someone_likes_baseball
    (K : forall x, (Y x /\ H x) -> B x)
    (L : forall y, A y -> H y)
    (M : exists z, Y z /\ A z) :
    exists t, B t.
  Proof.
    destruct M as [z [Yz Az]].
    exists z. apply K. split; [assumption | apply (L _ Az)].
  Qed.

  Context (j : U) (Pes Vyje Kocka Mys PSS : U -> Prop) (Ma : U -> U -> Prop).

  Lemma jan_chill
        (PV : forall p : U, Pes p -> Vyje p)
        (MKNM : forall p : U, (exists c, Kocka c /\ Ma p c) -> forall m : U, Mys m -> ~ Ma p m)
        (MPSS : forall p : U, PSS p -> (forall t : U, Vyje t -> ~ (Ma p t)))
        (JK : exists c : U, Kocka c /\ Ma j c) :
    PSS j -> (forall m : U, Mys m -> ~ (Ma j m)).
  Proof.
    intros JPSS m mys.
    apply MKNM.
    - apply JK.
    - apply mys.
  Qed.

End slovni_uloha.
