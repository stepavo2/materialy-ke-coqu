(** Induktivni definice sudych prirozenych cisel: *)
Inductive ev : nat -> Prop :=
  | ev_0 : ev 0
  | ev_SS : forall (n : nat), ev n -> ev (S (S n)).

(**Dokazte, ze 4 je suda: *)
Lemma ev_4 : ev 4.
Proof.
  apply ev_SS.
  apply ev_SS.
  apply ev_0.
Qed.

Print ev_4.

Fixpoint double (n : nat) : nat :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

(**Dokazte, ze dvojnasobek kazdeho cisla
je cislo sude.*)
Lemma ev_double : forall (n : nat), ev (double n).
Proof.
  intro n.
  induction n.
  - simpl.
    apply ev_0.
  - simpl.
    apply ev_SS.
    assumption.
Qed.


(** Dokazte "inverzni" lemma: *)
Lemma ev_inversion :
  forall (n : nat), ev n ->
  (n = 0) \/ (exists n', n = S (S n') /\ ev n').
Proof.
  intros n H.
  destruct H.
  - left. reflexivity.
  - right. exists n. split.
    + reflexivity.
    + apply H.
Qed.


(* Dokazte (staci [destruct]):*)
Lemma ev_minus2 :
  forall (n : nat), ev n -> ev (pred (pred n)).
Proof.
  intros n H.
  destruct H.
  - simpl. apply ev_0.
  - simpl. apply H.
Qed.

(* Dokazte, na predpoklad uzijte [ev_inversion]
([apply ev_inversion in H]).
*)
Lemma evSS_ev :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
  intros n H.
  apply ev_inversion in H as [].
  - discriminate H.
  - destruct H as (n' & Essn & Hevn').
    injection Essn as En. rewrite En. apply Hevn'.
Qed.

(** Jednodussi zpusob je pouzit taktiku [inversion].
  Ta automatizuje praci, kterou jsme delali manualne. *)
Lemma evSS_ev' :
  forall (n : nat), ev (S (S n)) -> ev n.
Proof.
  intros n H.
  inversion H. subst. apply H1.
Qed.

(** Dokazte, ze 1 neni suda.
([inversion] pomaha.) *)
Lemma one_not_ev : ~ ev 1.
Proof.
  intros contra. inversion contra.
Qed.

(** Dokazte s vyuzitim [inversion] :*)
Theorem SSSSev__even : forall n,
  ev (S (S (S (S n)))) -> ev n.
Proof.
  intros n H.
  inversion H; inversion H1; subst. apply H3.
Qed.

(** Dokazte s vyuzitim [inversion] :*)
Theorem ev5_nonsense :
  ev 5 -> 2 + 2 = 9.
Proof.
  
  intros H. inversion H. inversion H1. inversion H3.
Qed.

(** Alternativni, "neinduktivni" definice sudosti: *)
Definition Even x := exists n : nat, x = double n.

(** Obcas je treba pouzit indukci podle predikatu: *)
(** (nezapomenme na [unfold]!) *)
Lemma ev_Even : forall n,
  ev n -> Even n.
Proof.
  intros n H.
  induction H.
  - unfold Even.
    exists 0.
    reflexivity.
  - unfold Even in *.
    destruct IHev.
    rewrite H0.
    exists (S x).
    reflexivity.
Qed.

(** Dokazte indukci podle predikatu: *)
Lemma ev_sum : forall (n m : nat), ev n -> ev m -> ev (n + m).
Proof.
  intros n m Hn Hm.
  induction Hn.
  - simpl. apply Hm.
  - simpl. apply ev_SS. apply IHHn.
Qed.


(** Dokazte obraceny smer ekvivalence nasich dvou
definici sudosti. *)
Lemma Even_ev : forall n, Even n -> ev n.
Proof.
  intros _ [n ->].
  induction n.
  - simpl. constructor.
  - simpl. constructor. assumption.
Qed.

(** Zde je dalsi mozna definice sudosti: *)
Inductive ev' : nat -> Prop :=
  | ev'_0 : ev' 0
  | ev'_2 : ev' 2
  | ev'_sum : forall (n m : nat),
              ev' n -> ev' m -> ev' (n + m).

(** Dokazte ekvivalenci techto dvou
induktivnich definic. *)
Lemma ev'_ev : forall (n : nat), ev' n <-> ev n.
Proof.
  split. {
    intros H.
    induction H.
    - constructor.
    - constructor. constructor.
    - apply ev_sum. apply IHev'1. apply IHev'2.
  } {
    intros H.
    induction H.
    - constructor.
    - assert (S (S n) = 2 + n). reflexivity.
      rewrite H0.
      constructor.
      + constructor.
      + apply IHev.
  }
Qed.

(** Dokazte indukci (ale podle ceho?): *)
Theorem ev_ev__ev : forall n m,
  ev (n+m) -> ev n -> ev m.
Proof.
  intros n m Hnm Hn.
  induction Hn.
  - simpl in Hnm. assumption.
  - apply IHHn. simpl in Hnm. inversion Hnm. assumption.
Qed.

(** Navrat k logice. *)

(** V Coqu jsou logicke spojky representovany
jako induktivni typy.
Vyjimky:
- obecna kvantifikace (dependent product)
- implikace (function type)
- negace (simulovano pomoci implikace a False) *)

(** Jak definovat [True]? *)
Inductive True : Prop :=
  | I : True.

(** [True] ma jediny konstruktor [I].
[I] je (jedinym) dukazem vyroku [True]. *)

Check I.

(** Jak definovat [False]? *)
Inductive False : Prop :=
  .

(** [False] nema zadny konstruktor!
Samozrejme: nechceme mit zpusob, jak
dokazat [False]. *)

(** Jak definovat konjunkci [and]? *)
Inductive and (A B : Prop) : Prop :=
  conj : A -> B -> and A B.

(** [and] ma jediny konstruktor:
[conj] ocekava dukaz vyroku [A] a dukaz vyroku [B],
rekneme [a : A] a [b : B] a umoznuje
sestrojit dukaz vyroku [and A B],
ktery se nazyva [conj A B a b].*)

Section konjunkce_priklad.

Context (A B : Prop) (a : A) (b : B).

Check conj A B a b.

End konjunkce_priklad.

Print or.

(** Jak definovat disjunkci [or]? *)
Inductive or (A B : Prop) : Prop :=
  | or_introl : A -> or A B
  | or_intror : B -> or A B.

(** [or] ma dva konstruktory:
[or_introl] ocekava dukaz vyroku [A]
a umoznuje sestrojit dukaz vyroku [or A B],
[or_intror] ocekava dukaz vyroku [B]
a umoznuje sestrojit dukaz vyroku [or A B].*)

Section disjunkce_priklad.

Context (A B C: Prop) (a : A) (b : B).

Check or_introl A C a.

Check or_intror A B b.

End disjunkce_priklad.

(** Jak definovat existencni kvantifikator? *)
Inductive ex (A : Type) (P : A -> Prop) : Prop :=
  | ex_intro (x: A) (p : P x) : ex A P.

(** Mame-li typ [A] a vlastnost [P : A -> Prop],
[ex A P] ma byt vyrok tvrdici
"existuje prvek z A s vlastnosti P".
Jak (konstruktivne) dokazat,
ze nejaky takovy prvek existuje?
Nejprve musime predlozit prvek [x : A],
a pote musime predlozit dukaz, ze
[x] ma vlastnost [P], tedy nejaky dukaz [p : P x].
To staci k sestrojeni dukazu tvrzeni [ex A P].

Presne to umoznuje konstruktor [ex_intro].
*)

Section existence_priklad.

Check ev_4.
Print ev_4.

Check ex_intro _ _ 4 ev_4.

(** Existuje sude cislo: *)
Definition exists_even_nat : ex nat ev :=
  ex_intro nat ev 4 ev_4.

End existence_priklad.

(** Dokonce i rovnost lze definovat induktivne: *)
Inductive equal (A : Type) : A -> A -> Prop :=
  equal_refl : forall x : A, equal A x x.

(** Poznamka: toto neni nejjednodussi definice
rovnosti; Coq interne pouziva mirne jinou.*)

Section logika_priklady.

Context (A B C P Q R S : Prop).

(** Vyreste stare zname ulohy.
Napiste primo dukazove termy, tj. programy
odpovidajici dukazum danych tvrzeni. *)

(** Pokud chcete, muzete ucinit
"proposicni" argumenty implicitnimi:
Arguments conj {A B}.
Arguments or_introl {A B}.
Arguments or_intror {A B}.
*)
Definition and_assoc : (and P (and Q R)) -> (and (and P Q) R) :=
  fun prf =>
    match prf with
    | conj _ _ p qr =>
        match qr with
        | conj _ _ q r => conj (and P Q) R (conj P Q p q) r
        end
    end.

Definition and_imp_dist : (and (P -> Q) (R -> S)) ->
                          ((and P R) -> (and Q S)) :=
  fun pqrs =>
    match pqrs with
    | conj _ _ pq rs =>
        fun pr =>
          match pr with
          | conj _ _ p r => conj _ _ (pq p) (rs r)
          end
    end.

Definition uncurrying : (A -> (B -> C)) -> ((and A B) -> C) :=
  fun abc =>
  fun ab =>
    match ab with
    | conj _ _ a b => abc a b
    end.

Definition currying : ((and A B) -> C) -> (A -> (B -> C)) :=
  fun abc =>
  fun a =>
  fun b => abc (conj _ _ a b).

Definition or_comm : (or A B) -> (or B A) :=
  fun ab =>
    match ab with
    | or_introl _ _ a => or_intror _ _ a
    | or_intror _ _ b => or_introl _ _ b
    end.

Definition either_implies :
  ((or A B) -> C) -> (and (A -> C) (B -> C)) :=
  fun oabc =>
    conj _ _ (fun a => oabc (or_introl _ _ a)) (fun b => oabc (or_intror _ _ b)).

End logika_priklady.

Example lt_3_5 : 3 < 5.
Proof. unfold lt. apply le_S. apply le_n. Qed. 

Definition three_in_subset := exist (fun x => x < 5) 3 lt_3_5.

Definition extract : forall (X : Set) (P : X -> Prop),
    { x : X | P x } -> X :=
  fun _ _ s =>
    match s with
    | exist _ x _ => x
    end.

Compute (extract _ _ three_in_subset).

Definition eq_dec (A: Set) := forall x y : A, {x = y} + {~ x = y}.

Lemma nat_eq_dec : eq_dec nat.
Proof.
  unfold eq_dec.
  intros x.
  induction x.
  - intros [].
    + left. reflexivity.
    + right. discriminate.
  - intros [].
    + right. discriminate.
    + destruct (IHx n).
      * left. rewrite e. reflexivity.
      * right. intros contra. injection contra as contra. contradiction.
Qed.


Section rovnost_priklady.

Context (A : Type).

Definition equal_reflexive : forall a : A, equal A a a.
Admitted.

Definition equal_symmetric : forall a b : A,
  equal A a b -> equal A b a.
Admitted.

Definition eq_transitive : forall a b c : A,
  equal A a b -> equal A b c -> equal A a c.
Admitted.

End rovnost_priklady.
