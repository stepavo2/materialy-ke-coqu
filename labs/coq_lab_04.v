Print nat.

Print Nat.add.
Compute (Nat.add 2 4).
(** Misto [Nat.add] lze psat i [plus] (zkracena notace). *)
Compute (plus 2 4).

(** Lze take pouzivat infixni notaci [_ + _]. *)
Compute 2 + 4.

(** Pomoci [Locate] lze hledat definice notaci. *)
Locate "_ + _".


Print Nat.mul.
Compute (Nat.mul 2 4).
(** misto [Nat.mul] lze psat i [mult] *)
Compute (mult 2 4).

(** Lze take pouzivat infixni notaci [_ * _]. *)
Compute 2 * 4.
Locate "_ * _".

(** Definice dostupne z modulu [Nat]: *)
Print Nat.

(** Dokazte jednoducha tvrzeni plynouci
primo z definic operaci [+] a [*]: *)
Lemma plus_O_n : forall n : nat, 0 + n = n.
Proof.
  intros n.
  simpl. reflexivity.
Qed.

Lemma mult_0_l : forall n : nat, 0 * n = 0.
Proof.
  intros n.
  simpl. reflexivity.
Qed.

(** Dokazte pomocna tvrzeni o scitani
a o nasobeni indukci (taktika [induction]): *)
Lemma plus_n_O : forall n : nat, n + 0 = n.
Proof.
  intros n.
  induction n as [| n' IHn'].
  - simpl. reflexivity.
  - simpl. rewrite IHn'. reflexivity.
Qed.

Lemma mult_n_O : forall n : nat, n * 0 = 0.
Proof.
  intros n.
  induction n as [| n' IHn'].
  - reflexivity.
  - simpl. assumption.
Qed.

Lemma plus_n_Sm : forall n m : nat, S (n + m) = n + S m.
Proof.
  intros n m.
  induction n as [| n' IHn'].
  - simpl. reflexivity.
  - simpl. rewrite IHn'. reflexivity.
Qed.

Lemma plus_Sn_m : forall n m : nat, S n + m = S (n + m).
Proof.
  intros n m.
  simpl. reflexivity.
Qed.

(** Formulujte a dokazte zakladni vlastnosti
operaci scitani a nasobeni: komutativitu, asociativitu, distributivitu. *)
(** Muzete pouzivat jiz dokazana tvrzeni pomoci [rewrite]
(a v druhem smeru [rewrite <-]). *)
(** Muzete si pomoci dokazovanim pomocnych tvrzeni (lemmat). *)
(** Obcas nepotrebujete vyuzit indukcni predpoklad.
V takovych pripadech lze nahradit taktiku [induction]
tak *)
(** tikou [destruct], ktera indukcni predpoklad nezavede. *)

Theorem plus_commutativity : forall n m : nat, n + m = m + n.
Proof.
  intros n.
  induction n as [ | n' IHn'].
  (* ^ V zakladnim kroku nezavadime nove identifikatory.
  V indukcnim kroku oznacujeme predchudce [n] jako [n']
  a indukcni predpoklad jako [IHn']
  (jmena si muzete sami zvolit). *)
  - intros m.
    simpl.
    rewrite plus_n_O.
    reflexivity.
  - intros m.
    simpl.
    rewrite <- plus_n_Sm.
    rewrite IHn'.
    reflexivity.
Qed.

Theorem plus_associativity : forall m n o : nat,
    (m + n) + o = m + (n + o).
Proof.
  intros m n o.
  induction m as [| m' IHm'].
  - simpl. reflexivity.
  - simpl. rewrite IHm'. reflexivity.
Qed.

Theorem plus_mult_distr : forall m n o : nat,
    m * (n + o) = m * n + m * o.
Proof.
  intros m n o.
  induction m as [| m' IHm'].
  - simpl. reflexivity.
  - simpl.
    rewrite IHm'.
    rewrite plus_associativity.
    assert (H1 : o + (m' * n + m' * o) = (o + m' * n) + m' * o). {
      rewrite plus_associativity. reflexivity.
    }
    rewrite H1.
    assert (H2 : o + m' * n = m' * n + o). {
      rewrite plus_commutativity. reflexivity.
    }
    rewrite H2.
    rewrite plus_associativity.
    rewrite <- plus_associativity.
    reflexivity.
Qed.


(** Hezke cviceni prebrane z Logical Foundations: *)

(** Pripomenuti funkce "mensi nebo rovno?"
a "rovno?" *)
Fixpoint leb (n m : nat) : bool :=
  match n with
  | O => true
  | S n' =>
      match m with
      | O => false
      | S m' => leb n' m'
      end
  end.

Fixpoint eqb (n m : nat) : bool :=
  match n with
  | O => match m with
         | O => true
         | S m' => false
         end
  | S n' => match m with
            | O => false
            | S m' => eqb n' m'
            end
  end.

(** Infixni notace pro [leb] a [eqb]. *)
Notation "x <=? y" := (leb x y) (at level 70) : nat_scope.
Notation "x =? y" := (eqb x y) (at level 70) : nat_scope.

(**
U kazdeho z nasledujich tvrzeni si *nejprve* promyslete, zda
1. pujde dokazat jen pomoci [simpl] a [rewrite],
2. bude nutny rozbor pripadu [destruct],
3. bude nutna indukce [induction].

*Potom* zkuste dane tvrzeni dokazat. *)

Theorem leb_refl : forall n : nat,
  (n <=? n) = true.
Proof.
Admitted.

Theorem zero_neqb_S : forall n:nat,
  0 =? (S n) = false.
Proof.
Admitted.

Theorem andb_false_r : forall b : bool,
  andb b false = false.
Proof.
Admitted.

Theorem plus_leb_compat_l : forall n m p : nat,
  n <=? m = true -> (p + n) <=? (p + m) = true.
Proof.
Admitted.

Theorem S_neqb_0 : forall n:nat,
  (S n) =? 0 = false.
Proof.
Admitted.

Theorem mult_1_l : forall n:nat, 1 * n = n.
Proof.
Admitted.

Theorem all3_spec : forall b c : bool,
  orb
    (andb b c)
    (orb (negb b)
         (negb c))
  = true.
Proof.
Admitted.
